package pl.poznan.iilo.adamkucz.year2023.dnd.spell;

public enum MagitType {
    ALTERATION, CONJURATION_SUMMONING, ENCHANTMENT_CHARM, NECROMANTIC, DIVINATION, ABJURATION, INVOCATION;
}

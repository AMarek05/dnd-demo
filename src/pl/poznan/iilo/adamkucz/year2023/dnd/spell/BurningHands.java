package pl.poznan.iilo.adamkucz.year2023.dnd.spell;

import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;
import pl.poznan.iilo.adamkucz.year2023.dnd.spell.aoe.Sector;

public class BurningHands extends Spell {
    public BurningHands(DndCharacter caster) {
        super("AffectNormalFires", MagitType.ALTERATION, 1, 0, "1r",
                new Sector(3, 120),
                "VS", "1s", SavingThrow.NONE, caster);
    }

}

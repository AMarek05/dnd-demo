package pl.poznan.iilo.adamkucz.year2023.dnd.spell;

import pl.poznan.iilo.adamkucz.year2023.dnd.DndBeing;
import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;
import pl.poznan.iilo.adamkucz.year2023.dnd.action.Action;
import pl.poznan.iilo.adamkucz.year2023.dnd.action.IncreaseFire;
import pl.poznan.iilo.adamkucz.year2023.dnd.action.ReduceFire;
import pl.poznan.iilo.adamkucz.year2023.dnd.spell.aoe.Circle;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Distance;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Duration;

public class AffectNormalFires extends Spell {
    public AffectNormalFires(DndCharacter caster) {
        super("AffectNormalFires", MagitType.ALTERATION, 1, 0.5, "1r", new Circle(3),
                "VS", "1s", SavingThrow.NONE, caster);
    }

    @Override
    public Distance getRange() {
        return Distance.ofIndoor(0.5 * getCaster().getLevel());
    }

    @Override
    public Duration getDuration() {
        return Duration.fromRounds(getCaster().getLevel());
    }

    @Override
    public boolean doAction(DndBeing self, Action action) {
        if (action instanceof ReduceFire) {
            ReduceFire rf = (ReduceFire) action;
            rf.target.reduce();
            return true;
        }
        if (action instanceof IncreaseFire) {
            IncreaseFire rf = (IncreaseFire) action;
            rf.target.increase();
            return true;
        }
        return super.doAction(self, action);
    }
}

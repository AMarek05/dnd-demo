package pl.poznan.iilo.adamkucz.year2023.dnd.spell.aoe;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Distance;

public class Circle extends AreaOfEffect {
    public final Distance radius;

    public Circle(Distance radius) {
        this.radius = radius;
    }

    public Circle(double radius) {
        this(Distance.ofIndoor(radius));
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.spell;

import pl.poznan.iilo.adamkucz.year2023.dnd.DndBeing;
import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;
import pl.poznan.iilo.adamkucz.year2023.dnd.action.Action;
import pl.poznan.iilo.adamkucz.year2023.dnd.spell.aoe.AreaOfEffect;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Distance;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Duration;

public class Spell {
    private final String name;
    private final MagitType[] magicType;
    private final int level;
    private final Distance range;
    private final Duration duration;

    private final AreaOfEffect areaOfEffect;

    private final Components components;
    private final Duration castingTime;
    private final SavingThrow savingThrow;

    private final DndCharacter caster;

    public String getName() {
        return name;
    }

    public MagitType[] getMagicType() {
        return magicType;
    }

    public int getLevel() {
        return level;
    }

    public Distance getRange() {
        return range;
    }

    public Duration getDuration() {
        return duration;
    }

    public AreaOfEffect getAreaOfEffect() {
        return areaOfEffect;
    }

    public Components getComponents() {
        return components;
    }

    public Duration getCastingTime() {
        return castingTime;
    }

    public SavingThrow getSavingThrow() {
        return savingThrow;
    }

    public DndCharacter getCaster() {
        return caster;
    }

    protected Spell(String name, MagitType[] magicType, int level, Distance range, Duration duration,
                    AreaOfEffect areaOfEffect, Components components, Duration castingTime, SavingThrow savingThrow,
                    DndCharacter caster) {
        this.name = name;
        this.level = level;
        this.magicType = magicType;
        this.range = range;
        this.duration = duration;
        this.areaOfEffect = areaOfEffect;
        this.components = components;
        this.castingTime = castingTime;
        this.savingThrow = savingThrow;
        this.caster = caster;
    }

    protected Spell(String name, MagitType[] magicType, int level, double range, String duration,
                    AreaOfEffect areaOfEffect, String componentCode, String castingTime, SavingThrow savingThrow,
                    DndCharacter caster) {
        this(name, magicType, level, Distance.ofIndoor(range), Duration.parseDuration(duration),
                areaOfEffect, new Components(componentCode), Duration.parseDuration(castingTime), savingThrow, caster);
    }

    protected Spell(String name, MagitType magicType, int level, double range, String duration,
                    AreaOfEffect areaOfEffect, String componentCode, String castingTime, SavingThrow savingThrow,
                    DndCharacter caster) {
        this(name, new MagitType[]{magicType}, level, Distance.ofIndoor(range), Duration.parseDuration(duration),
                areaOfEffect, new Components(componentCode), Duration.parseDuration(castingTime), savingThrow, caster);
    }

    public boolean doAction(DndBeing self, Action action) {
        return false;
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.spell;

import pl.poznan.iilo.adamkucz.year2023.dnd.item.Item;

import java.util.Optional;

public class Components {
    private final boolean verbal;
    private final boolean somatic;
    private final Optional<Item> material;

    public Components(boolean verbal, boolean somatic, Optional<Item> material) {
        this.verbal = verbal;
        this.somatic = somatic;
        this.material = material;
    }

    public Components(String code, Item material) {
        this(code.contains("V"), code.contains("S"), Optional.of(material));
    }

    public Components(String code) {
        this(code.contains("V"), code.contains("S"), Optional.empty());
        if (code.contains("M")) {
            throw new IllegalArgumentException("Required items need to be stated when a spell has an M component!");
        }
    }
}

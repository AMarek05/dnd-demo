package pl.poznan.iilo.adamkucz.year2023.dnd.util;

import java.util.Arrays;
import java.util.Random;

public class Util {
    public static final Random randomNumberGenerator = new Random();

    public static String capitalise(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static <T> boolean hasElement(T[] array, T element) {
        // functionally (advanced), the following line suffices
        // return Arrays.asList(array).contains(element)
        boolean alreadyKnown = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(array)) {
                return true;
            }
        }
        return false;

    }

    public static <T> T[] extendArray(T[] oldArray, T extraElement) {
        T[] newArray = Arrays.copyOf(oldArray, oldArray.length + 1);
        newArray[oldArray.length] = extraElement;
        return newArray;
    }

    public static <T> T[] removeFromArray(T[] oldArray, T elemToRemove) {
        T[] newArray = Arrays.copyOf(oldArray, oldArray.length - 1);
        for (int i = 0; i < newArray.length; i++) {
            if (newArray[i].equals(elemToRemove)) {
                System.arraycopy(oldArray, i + 1, newArray, i, oldArray.length - i);
                break;
            }
        }
        return newArray;
    }

    public static <T> T randomChoice(T[] elements) {
        return elements[randomNumberGenerator.nextInt(elements.length)];
    }

    public static void output(String message) {
        System.out.println(message);
    }
}

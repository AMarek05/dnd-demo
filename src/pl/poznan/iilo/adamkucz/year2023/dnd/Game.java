package pl.poznan.iilo.adamkucz.year2023.dnd;

import pl.poznan.iilo.adamkucz.year2023.dnd.action.HideInShadows;
import pl.poznan.iilo.adamkucz.year2023.dnd.monster.Monster;
import pl.poznan.iilo.adamkucz.year2023.dnd.monster.Orc;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Coordinates;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Field;

public class Game {
    public static void main(String[] args) {
        // this attempts to recreate in code the example combat from page 105
        Field field = new Field(new Coordinates(0, 0, 0));

        CharacterAbility standardAbilities = new CharacterAbility(5, 5, 5, 5, 5, 5);
        DndCharacter malcolm = DndCharacter.createCharacterWithAbilities("Malcolm the Chaotic Good MagicUser", standardAbilities);
        // TODO: implement Cleric
        DndCharacter clarisse = DndCharacter.createCharacterWithAbilities("Clarisse the Lawful Neutral Cleric", standardAbilities);
        DndCharacter tristan = DndCharacter.createCharacterWithAbilities("Tristan the True Neutral Thief", standardAbilities);
        DndCharacter felicia = DndCharacter.createCharacterWithAbilities("Felicia the Neutral Evil Fighter", standardAbilities);
        DndCharacter ferdinand = DndCharacter.createCharacterWithAbilities("Ferdinand the Lawful Evil Fighter", standardAbilities);
        DndParty party = new DndParty(malcolm, clarisse, tristan, felicia, ferdinand);
        party.moveTo(field.at(0, 0));

        MonsterGroup enemies = new MonsterGroup();
        // TODO: implement Illusionist
        DndCharacter ivan = DndCharacter.createCharacterWithAbilities("Ivan the Chaotic Evil Illusionist", standardAbilities);
        Orc[] orcs = new Orc().copy(20);
        enemies.add(ivan);
        // TODO: implement Monster and Orc
        enemies.addAll(orcs);
        enemies.moveTo(field.at(30, 0));

        // TODO: implement actions: MemoriseSpell for MagicUser and PrayForSpell for Cleric
        malcolm.doAction(new MemoriseSpell(new Sleep()));
        clarisse.doAction(new PrayForSpell(new Silence()));

        ivan.doAction(new MemoriseSpell(new PrismaticSpray()));

        // TODO: implement action CastSpell
        malcolm.doAction(new CastSpell("sleep"));
        clarisse.doAction(new CastSpell("silence"));
        tristan.moveTo(field.at(-2,0));
        tristan.doAction(new HideInShadows(HideInShadows.NAME));
        // TODO: implement combat actions Shoot and Hurl
        felicia.doAction(new Shoot(ivan));
        ferdinand.doAction(new Hurl(ferdinand.getItem("axe"), ivan));

        ivan.doAction(new CastSpell("prismatic spray"));
        // so-called enhanced for or 'foreach'
        for (Orc orc : orcs) {
            orc.doAction(new Hurl(orc.getItem("spear"), party.randomMember()));
            orc.moveTo(field.at(0, 0));
        }
    }
}

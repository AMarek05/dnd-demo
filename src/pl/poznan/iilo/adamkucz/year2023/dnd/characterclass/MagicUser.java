package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

import pl.poznan.iilo.adamkucz.year2023.dnd.Alignment;
import pl.poznan.iilo.adamkucz.year2023.dnd.CharacterAbility;
import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;
import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;
import pl.poznan.iilo.adamkucz.year2023.dnd.item.SpellBook;

public class MagicUser extends CharacterClass {
    private static final String[] titles = {"", "Prestidigitator", "Evoker", "Conjurer", "Theurgist", "Thaumaturgist",
            "Magician", "Enchanter", "Warlock", "Sorcerer", "Necromancer", "Wizard"};
    private static final int[] maxExpLevels = {
            -1, 2_500, 5_000, 10_000, 22_500, 40_000, 60_000, 90_000, 135_000, 250_000};

    private static final int[][] spellGains = {
            {},
            {1, 2, 4, 5, 13, 26, 29},
            {3, 4, 7, 10, 13, 26, 29},
            {5, 6, 8, 11, 13, 26, 29},
            {7, 8, 11, 12, 15, 26, 29},
            {9, 10, 11, 12, 15, 27},
            {12, 13, 16, 20, 22, 27},
            {14, 16, 17, 21, 23, 27},
            {16, 17, 19, 21, 23, 28},
            {18, 20, 22, 24, 25, 28}
    };

    private SpellBook[] spellBooks;

    public MagicUser() {
        super("MagicUser", 4, Integer.MAX_VALUE, "2d4");
         spellBooks = new SpellBook[]{new SpellBook("Strange tome", "common tongue")};
    }

    @Override
    public boolean isAllowed(Alignment alignment, CharacterAbility ability) {
        return !(ability.intelligence < 9) && ability.dexterity >= 6;
    }

    @Override
    public double expModifier(CharacterAbility ability) {
        return ability.intelligence >= 16 ? 1.1 : 1;
    }

    @Override
    public void applyInitialSpecialModifiers(DndCharacter character) {
        for (int i = 0; i < spellBooks.length; i++) {
            character.addToInventory(spellBooks[i]);
        }
    }

    @Override
    public int getMaxExpOfLevel(int level) {
        return level < maxExpLevels.length ? maxExpLevels[level] : (375_000 * (level + 1 - maxExpLevels.length));
    }

    @Override
    public String getTitle(int level) {
        if (level < titles.length) {
            return titles[level];
        }
        if (level == 16) {
            return  "Mage";
        }
        if (level == 18) {
            return "Arch-Mage";
        }
        return titles[titles.length - 1] + " (" + level + "th level)";
    }

    @Override
    public void levelUp(DndCharacter self) {
        self.increaseMaxHitPoints(self.getLevel() <= 11 ? Dice.roll(hitDieType) : 1);
    }

    public int numberOfUsableSpells(int level, int spellLevel) {
        // functional (advanced)
        // return (int) Arrays.stream(spellGains[spellLevel])
        //                    .takeWhile(req -> req <= level)
        //                    .count();
        int[] levelsWhenGained = spellGains[spellLevel];
        for (int i = 0; i < levelsWhenGained.length; i++) {
            if (levelsWhenGained[i] > level) {
                return i;
            }
        }
        return 0;
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

import pl.poznan.iilo.adamkucz.year2023.dnd.*;
import pl.poznan.iilo.adamkucz.year2023.dnd.action.Action;
import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;

public class CharacterClass {
    public final String name;
    public final int hitDieType;
    public final int levelLimit;
    private final String startingMoneyDice;

    protected CharacterClass(String name, int hitDieType, int levelLimit, String startingMoneyDice) {
        this.name = name;
        this.hitDieType = hitDieType;
        this.levelLimit = levelLimit;
        this.startingMoneyDice = startingMoneyDice;
    }

    public boolean isAllowed(Alignment alignment, CharacterAbility ability) {
        return true;
    }

    public int getLevel(int experience) {
        int level = 0;
        while (experience > getMaxExpOfLevel(level)) {
            level++;
        }
        return level;
        // functionally (advanced), the following line is enough
        // return IntStream.iterate(1, l -> l + 1)
        //                 .takeWhile(l -> getExpNeededForLevel(l) <= experience)
        //                 .max().getAsInt();
    }

    public int getMaxExpOfLevel(int level) {
        return Integer.MAX_VALUE;
    }

    public void applyInitialSpecialModifiers(DndCharacter character) {};

    public String getTitle(int level) {
        return "None";
    }

    public double expModifier(CharacterAbility ability) {
        return 1;
    }

    public int rollStartingMoney() {
        return Dice.roll(startingMoneyDice) * 10;
    }

    // assumes the level returned by self.getLevel() is the newly advanced to level
    public void levelUp(DndCharacter self) {
        self.increaseMaxHitPoints(Dice.roll(hitDieType));
    };

    public String[] knownActions() {
        return new String[]{};
    }

    // returns true if the action is done and no further handlers should be used
    public boolean doAction(DndCharacter self, Action action) {
        return false;
    }

    public int initialHp() {
        return Dice.roll(hitDieType);
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

public class Peasant extends CharacterClass {

    public Peasant() {
        super("Peasant", 6, Integer.MAX_VALUE, "0d1");
    }

    @Override
    public int getMaxExpOfLevel(int level) {
        return 5000 * (int) Math.pow(2, level - 1);
    }

    @Override
    public String getTitle(int level) {
        /*if (level < 10) {
            return "Peasant";
        } else {
            return "Agricultural developer";
        }*/
        // check ? val1 : val2
        // gives value of val1 if boolean check is true
        //   otherwise it gives val2
        return level < 10 ? "Peasant" : "Agricultural developer";
    }
}

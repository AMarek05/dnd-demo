package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

import pl.poznan.iilo.adamkucz.year2023.dnd.*;
import pl.poznan.iilo.adamkucz.year2023.dnd.action.*;
import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;
import pl.poznan.iilo.adamkucz.year2023.dnd.item.Book;
import pl.poznan.iilo.adamkucz.year2023.dnd.item.Item;
import pl.poznan.iilo.adamkucz.year2023.dnd.util.Util;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.*;

import java.util.Arrays;
import java.util.Optional;

public class Thief extends CharacterClass {
    private static final String[] titles = {"", "Rogue (Apprentice)",
            "Footpad", "Cutpurse", "Robber", "Burglar", "Filcher", "Sharper", "Magsman", "Thief", "Master Thief"};
    private static final int[] maxExpLevels = {
            -1, 1_250, 2_500, 5_000, 10_000, 20_000, 42_500, 70_000, 110_000, 160_000};

    public Thief() {
        super("Thief", 6, Integer.MAX_VALUE, "2d6");
    }

    @Override
    public boolean isAllowed(Alignment alignment, CharacterAbility ability) {
        return !(ability.dexterity < 9) && (alignment.order.equals("neutral") || alignment.ethics.equals("evil"));
    }

    @Override
    public double expModifier(CharacterAbility ability) {
        return ability.dexterity > 15 ? 1.1 : 1;
    }

    @Override
    public void applyInitialSpecialModifiers(DndCharacter character) {
        character.learnLanguage("Thieves' Cant");
    }

    @Override
    public int getMaxExpOfLevel(int level) {
        return level < maxExpLevels.length ? maxExpLevels[level] : (220_000 * (level + 1 - maxExpLevels.length));
    }

    @Override
    public String getTitle(int level) {
        return level < titles.length ? titles[level] : (titles[titles.length - 1] + " (" + level + "th level)");
    }

    private Lock[] locksAttempted = new Lock[]{};
    private Location[] locationsSearchedForTraps = new Location[]{};
    private Trap[] trapsAttempted = new Trap[]{};
    private String[] languagesAttempted = new String[]{};

    @Override
    public void levelUp(DndCharacter self) {
        self.increaseMaxHitPoints(self.getLevel() <= 10 ? Dice.roll(hitDieType) : 2);
        this.locksAttempted = new Lock[]{};
        this.languagesAttempted = new String[]{};
    }

    private PickPocketResult pickPockets(int level, DndCharacter victim, Optional<Item> target) {
        int[] maxAcceptableArr = {0, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 90, 100, 105, 110, 115, 120};
        int maxAcceptable = level < maxAcceptableArr.length ? maxAcceptableArr[level] : 125;
        if (victim.getLevel() > 3) {
            maxAcceptable -= 5 * (victim.getLevel() - 3);
        }

        int attemptScore = Dice.roll(100);
        if (attemptScore <= maxAcceptable && target.isPresent() && victim.hasItem(target.get())) {
            return new PickPocketResult(target.get());
        } else if (attemptScore >= maxAcceptable + 21) {
            return new PickPocketResult(PickPocketResult.NOTICED);
        }
        return new PickPocketResult(PickPocketResult.FAILURE);
    }

    public PickPocketResult pickPockets(int level, DndCharacter victim) {
        return pickPockets(level, victim, Optional.of(Util.randomChoice(victim.allItems())));
    }

    public PickPocketResult pickPockets(int level, DndCharacter victim, Item target, Container container) {
        if (container.contains(target)) {
            return pickPockets(level, victim);
        }
        return pickPockets(level, victim, Optional.of(Util.randomChoice(container.contents)));
    }

    public boolean openLock(int level, Lock lock) {
        if (Util.hasElement(locksAttempted, lock)) {
            return false;
        }
        locksAttempted = Util.extendArray(locksAttempted, lock);

        int[] maxAcceptableArr = {0, 25, 29, 33, 37, 42, 47, 52, 57, 62, 67, 72, 77, 82, 87, 92, 97};
        int maxAcceptable = level < maxAcceptableArr.length ? maxAcceptableArr[level] : 99;
        int attemptScore = Dice.roll(100);
        if (attemptScore <= maxAcceptable) {
            lock.open();
            return true;
        }
        return false;
    }

    public Trap[] findTraps(int level, Location loc) {
        if (Util.hasElement(locationsSearchedForTraps, loc)) {
            return new Trap[]{};
        }
        locationsSearchedForTraps = Util.extendArray(locationsSearchedForTraps, loc);

         int maxAcceptable = Math.min(15 + 5 * level, 99);
         int attemptScore = Dice.roll(100);
         if (attemptScore <= maxAcceptable) {
             return loc.traps();
         } else {
             return new Trap[]{};
         }
    }

    public boolean removeTrap(int level, Trap trap) {
        if (Util.hasElement(trapsAttempted, trap)) {
            return false;
        }
        trapsAttempted = Util.extendArray(trapsAttempted, trap);

        int maxAcceptable = Math.min(15 + 5 * level, 99);
        int attemptScore = Dice.roll(100);
        if (attemptScore <= maxAcceptable) {
            trap.deactivate();
            return true;
        }
        return false;
    }

    public boolean moveSilently(int level) {
        int[] maxAcceptableArr = {0, 15, 21, 27, 33, 40, 47, 55, 62, 70, 78, 86, 94};
        int maxAcceptable = level < maxAcceptableArr.length ? maxAcceptableArr[level] : 99;
        return Dice.roll(100) <= maxAcceptable;
    }

    public boolean hideInShadows(int level) {
        int[] maxAcceptableArr = {0, 10, 15, 20, 25, 31, 37, 43, 49, 56, 63, 70, 77, 85, 93};
        int maxAcceptable = level < maxAcceptableArr.length ? maxAcceptableArr[level] : 99;
        return Dice.roll(100) <= maxAcceptable;
    }

    public boolean listen(int level, Door door, Location loc) {
        int[] maxAcceptableArr = {0, 10, 10, 15, 15, 20, 20, 25, 25, 30, 30, 35, 35, 40, 40, 50, 50};
        int maxAcceptable = level < maxAcceptableArr.length ? maxAcceptableArr[level] : 55;
        // functionally (advanced)
        // return Dice.roll(100) <= maxAcceptable &&
        //        door.getOtherSide(loc).flatMap(door::getOtherSide).map(Location::noisy).orElse(false);
        Optional<Location> target = door.getOtherSide(loc);
        int attemptScore = Dice.roll(100);
        if (target.isPresent() && attemptScore <= maxAcceptable) {
            return target.get().noisy();
        }
        return false;
    }

    public boolean climbWall(int level) {
        int[] maxAcceptableArr = {0, 850, 860, 870, 880, 900, 920, 940, 960, 980, 990, 991, 992, 993, 994, 995, 996};
        int maxAcceptable = level < maxAcceptableArr.length ? maxAcceptableArr[level] : 997;
        return Dice.roll(1000) < maxAcceptable;
    }

    public boolean read(DndCharacter self, Book book) {
        String lang = book.language;
        if (self.knowsLanguage(lang)) {
            return true;
        }
        if (Util.hasElement(languagesAttempted, lang)) {
            return false;
        }
        languagesAttempted = Util.extendArray(languagesAttempted, lang);

        int lvl = self.getLevel();
        int maxAcceptable = lvl < 4 ? 0 : lvl < 16 ? 20 + 5 * (lvl - 4) : 80;
        int attemptScore = Dice.roll(100);
        if (attemptScore <= maxAcceptable) {
            self.learnLanguage(lang);
            return true;
        }
        return false;
    }

    @Override
    public String[] knownActions() {
        return new String[]{PickPockets.NAME, OpenLock.NAME, FindTraps.NAME, RemoveTrap.NAME, MoveSilently.NAME,
                HideInShadows.NAME, ListenAtDoor.NAME, ClimbWall.NAME, /*Backstab.NAME,*/ Read.NAME};
        // TODO: implement Backstab
    }

    @Override
    public boolean doAction(DndCharacter self, Action action) {
        if (action instanceof PickPockets) {
            PickPockets pp = (PickPockets) action;
            PickPocketResult result;
            if (pp.place.isPresent() && pp.target.isPresent()) {
                result = pickPockets(self.getLevel(), pp.victim, pp.target.get(), pp.place.get());
            } else {
                result = pickPockets(self.getLevel(), pp.victim);
            }
            if (result.type == PickPocketResult.SUCCESS) {
                Item loot = result.stolenItem.get();
                self.addToInventory(loot);
                pp.victim.removeFromInventory(loot);
            }
            return true;
        }
        if (action instanceof OpenLock) {
            OpenLock ol = (OpenLock) action;
            openLock(self.getLevel(), ol.target);
            return true;
        }
        if (action instanceof FindTraps) {
            FindTraps ft = (FindTraps) action;
            Trap[] traps = findTraps(self.getLevel(), ft.target);
            if (traps.length == 0) {
                Util.output("There are no traps here.");
            } else {
                Util.output("Found the following traps: " + Arrays.toString(traps));
            }
            return true;
        }
        if (action instanceof RemoveTrap) {
            RemoveTrap rt = (RemoveTrap) action;
            removeTrap(self.getLevel(), rt.target);
            return true;
        }
        if (action instanceof MoveSilently) {
            MoveSilently ms = (MoveSilently) action;
            boolean isSilent = moveSilently(self.getLevel());
            if (!isSilent) {
                self.doAction(new Move(ms.target));
            } else {
                self.moveTo(ms.target);
            }
            return true;
        }
        if (action instanceof HideInShadows) {
            hideInShadows(self.getLevel());
            return true;
        }
        if (action instanceof ListenAtDoor) {
            ListenAtDoor lad = (ListenAtDoor) action;
            boolean noise = listen(self.getLevel(), lad.target, self.getCurrentLocation());
            if (noise) {
                Util.output("");
            }
        }
        if (action instanceof ClimbWall) {
            ClimbWall cw = (ClimbWall) action;
            Optional<Location> destination = cw.target.getOtherSide(self.getCurrentLocation());
            if (destination.isPresent()) {
                boolean climbed = climbWall(self.getLevel());
                if (climbed) {
                    self.moveTo(destination.get());
                }
                return true;
            } else {
                throw new IllegalArgumentException("Not near the wall!");
            }
        }
        if (action instanceof Read) {
            Read r = (Read) action;
            read(self, r.target);
            return false;
        }
        return super.doAction(self, action);
    }
}

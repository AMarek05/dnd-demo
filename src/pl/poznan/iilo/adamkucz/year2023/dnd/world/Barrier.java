package pl.poznan.iilo.adamkucz.year2023.dnd.world;

import java.util.Optional;

public class Barrier extends WorldElement {
    public final Location side1, side2;

    public Barrier(Location side1, Location side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public Optional<Location> getOtherSide(Location side) {
        if (side1.equals(side)) {
            return Optional.of(side2);
        }
        if (side2.equals(side)) {
            return Optional.of(side1);
        }
        return Optional.empty();
    }
}

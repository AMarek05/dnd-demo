package pl.poznan.iilo.adamkucz.year2023.dnd.world;

public class Distance {
    public static final int OUTDOOR_MULTIPLIER = 3;
    private final double feet;

    private Distance(double feet) {
        this.feet = feet;
    }

    public double feet() {
        return feet(true);
    }

    public double feet(boolean indoor) {
        return indoor ? feet : OUTDOOR_MULTIPLIER * feet;
    }

    public static Distance ofIndoor(double feet) {
        return new Distance(feet);
    }

    public static Distance ofOutdoor(double feet) {
        return new Distance(feet / OUTDOOR_MULTIPLIER);
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.world;

import java.util.List;

public class Fire extends WorldElement {
    private Duration remainingFuel;
    private int modifiedSize;

    public Fire(Duration remainingFuel) {
        this.remainingFuel = remainingFuel;
        this.modifiedSize = 0;
    }

    public Duration getRemainingFuel() {
        return remainingFuel.mul(Math.pow(2, modifiedSize));
    }

    public Brightness getBrightness() {
        if (modifiedSize == 1) {
            return Brightness.LIGHT_SPELL;
        }
        if (modifiedSize == -1) {
            return Brightness.MATCH;
        }
        return Brightness.FIRE;
    }

    public void reduce() {
        modifiedSize = -1;
    }

    public void increase() {
        modifiedSize = 1;
    }

    public enum Brightness {
        MATCH, FIRE, LIGHT_SPELL;
    }
}

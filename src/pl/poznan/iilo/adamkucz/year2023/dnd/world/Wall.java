package pl.poznan.iilo.adamkucz.year2023.dnd.world;

public class Wall extends Barrier {

    public Wall(Location side1, Location side2) {
        super(side1, side2);
    }
}

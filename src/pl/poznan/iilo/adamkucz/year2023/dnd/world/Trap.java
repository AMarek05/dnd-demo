package pl.poznan.iilo.adamkucz.year2023.dnd.world;

public class Trap extends WorldElement {
    public boolean active;

    public void deactivate() {
        active = false;
    }
}

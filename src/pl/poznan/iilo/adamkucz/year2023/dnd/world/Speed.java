package pl.poznan.iilo.adamkucz.year2023.dnd.world;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Speed {
    public final int regular;

    // all of these are Integer because they can be null
    private final Integer fly;
    private final Integer swim;
    private final Integer burrow;
    private final Integer inWeb;


    public Speed(int regular, Integer fly, Integer swim, Integer burrow, Integer inWeb) {
        this.regular = regular;
        this.fly = fly;
        this.swim = swim;
        this.burrow = burrow;
        this.inWeb = inWeb;
    }

    public Speed(int regular) {
        this(regular, null, null, null, null);
    }

    public Speed(int regular, String code) {
        this.regular = regular;
        Pattern flyPattern = Pattern.compile("/(\\d+)");
        Pattern swimPattern = Pattern.compile("//(\\d+)");
        Pattern burrowPattern = Pattern.compile("\\((\\d+)\\)");
        Pattern inWebPattern = Pattern.compile("\\*(\\d+)");
        Matcher m = flyPattern.matcher(code);
        this.fly = m.find() ? Integer.valueOf(m.group(1)) : null;
        m.usePattern(swimPattern);
        this.swim = m.find() ? Integer.valueOf(m.group(1)) : null;
        m.usePattern(burrowPattern);
        this.burrow = m.find() ? Integer.valueOf(m.group(1)) : null;
        m.usePattern(inWebPattern);
        this.inWeb = m.find() ? Integer.valueOf(m.group(1)) : null;
    }

    public Optional<Integer> fly() {
        return Optional.ofNullable(fly);
    }

    public Optional<Integer> swim() {
        return Optional.ofNullable(swim);
    }

    public Optional<Integer> burrow() {
        return Optional.ofNullable(burrow);
    }

    public Optional<Integer> inWeb() {
        return Optional.ofNullable(inWeb);
    }
}

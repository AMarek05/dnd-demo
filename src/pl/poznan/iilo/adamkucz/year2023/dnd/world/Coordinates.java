package pl.poznan.iilo.adamkucz.year2023.dnd.world;

public class Coordinates {
    public final double x;
    public final double y;
    // z = 0 should indicate sea level
    public final double z;

    public Coordinates(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double straightLineDistanceTo(Coordinates coords) {
        // assume Euclidean space by default
        double dx = x - coords.x;
        double dy = y - coords.y;
        double dz = z - coords.z;
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    public Coordinates add(Coordinates other) {
        return new Coordinates(this.x + other.x, this.y + other.y, this.z + other.z);
    }
}

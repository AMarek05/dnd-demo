package pl.poznan.iilo.adamkucz.year2023.dnd.world;

public class Location extends WorldElement {
    private final Coordinates coords;

    // protected because no bare locations should be made by the user
    // each actual location should be an object of a subclass of Location, not the actual Location object
    protected Location(Coordinates coords) {
        this.coords = coords;
    }

    public Coordinates coords() {
        return coords;
    }

    public Trap[] traps() {
        return new Trap[]{};
    }

    public boolean noisy() {
        return false;
    }

    public Distance distanceTo(Location other) {
        return Distance.ofOutdoor(coords.straightLineDistanceTo(other.coords));
    }
}

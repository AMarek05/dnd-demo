package pl.poznan.iilo.adamkucz.year2023.dnd.world;

public class Duration {
    private static final int SEGMENTS_IN_DAY = 24 * 60 * 60 * 10;
    private final int segments;

    private Duration(int segments) {
        this.segments = segments;
    }

    public static Duration parseDuration(String duration) {
        if (!duration.matches("\\d+[srtd]")) {
            throw new IllegalArgumentException("Invalid format for distance");
        }
        String[] split = duration.split("[srtd]");
        int number = Integer.parseInt(split[0]);
        char code = duration.charAt(duration.length() - 1);
        if (code == 's') {
            return Duration.fromSegments(number);
        }
        if (code == 'r') {
            return Duration.fromRounds(number);
        }
        if (code == 't') {
            return Duration.fromTurns(number);
        }
        return Duration.fromDays(number);
    }

    public int segments() {
        return segments;
    }

    private int getUnit(int factor) {
        if (segments % factor != 0) {
            throw new IllegalArgumentException("Cannot convert non-multiple of 100 of segments into turns!");
        }
        return segments / factor;
    }

    public int rounds() {
        return getUnit(10);
    }

    public int minutes() {
       return rounds();
    }

    public int turns() {
        return getUnit(100);
    }

    public int days() {
        return getUnit(SEGMENTS_IN_DAY);
    }

    public static Duration fromSegments(int segments) {
        return new Duration(segments);
    }

    public static Duration fromRounds(int number) {
        return fromSegments(10 * number);
    }

    public static Duration fromTurns(int number) {
        return fromRounds(10 * number);
    }

    public static Duration fromDays(int number) {
        return fromSegments(SEGMENTS_IN_DAY);
    }

    public Duration mul(double factor) {
        return new Duration((int) (segments * factor));
    }
}

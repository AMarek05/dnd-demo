package pl.poznan.iilo.adamkucz.year2023.dnd.world;

import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;
import pl.poznan.iilo.adamkucz.year2023.dnd.item.Item;

import java.util.Optional;

public class World {
    private DndCharacter[] characters;
    private Item[] artifacts;

    // there is only one world, so it's a static WORLD constant
    public static final World WORLD = new World();
    private WorldElement[] everything;

    // no more wolrds besides WORLD should ever be created
    private World() {
        characters = new DndCharacter[]{};
        artifacts = new Item[]{};
        everything = new WorldElement[]{};
    }

    public Optional<DndCharacter> getByName(String name) {
        // functional (advanced)
        // return Arrays.stream(characters).filter(character -> character.getName().equals(name)).findAny();
        for (int i = 0; i < characters.length; i++) {
            if (characters[i].getName().equals(name)) {
                return Optional.of(characters[i]);
            }
        }
        return Optional.empty();
    }

    public Item getItem(String identifier) {
        for (int i = 0; i < artifacts.length; i++) {
            if (artifacts[i].getIdentifier().equals(identifier)) {
                return artifacts[i];
            }
        }
        return new Item(identifier);
    }

    public Optional<WorldElement> getElement(String spec) {
        // functional (advanced)
        // return Arrays.stream(everything).filter(elem -> elem.matches(spec)).findAny();
        for (int i = 0; i < everything.length; i++) {
            if (everything[i].matches(spec)) {
                return Optional.of(everything[i]);
            }
        }
        return Optional.empty();
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.WorldElement;

import java.util.Optional;

import static pl.poznan.iilo.adamkucz.year2023.dnd.world.World.WORLD;

public class TargetedAction<T extends WorldElement> extends Action {
    public final T target;

    public TargetedAction(String line) {
        super(line);
        if (arguments.length < 1) {
            throw new IllegalArgumentException("No place given to move to!");
        }
        Optional<WorldElement> specifiedFire = WORLD.getElement(arguments[0]);
        if (!specifiedFire.isPresent()) {
            throw new IllegalArgumentException("Invalid place specification for the place to move to!");
        }
        target = (T) specifiedFire.get();
    }

    public TargetedAction(String name, T target) {
        super(name + " " + target);
        this.target = target;
    }
}
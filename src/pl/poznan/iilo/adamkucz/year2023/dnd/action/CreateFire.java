package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Location;

public class CreateFire extends TargetedAction<Location> {
    public CreateFire(String line) {
        super(line);
    }

    public CreateFire(Location target) {
        super("createfire", target);
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Trap;

public class RemoveTrap extends TargetedAction<Trap> {
    public static final String NAME = "removetrap";

    public RemoveTrap(String line) {
        super(line);
    }

    public RemoveTrap(Trap target) {
        super(NAME, target);
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Location;

public class Move extends TargetedAction<Location> {
    public Move(String line) {
        super(line);
    }

    public Move(Location destination) {
        super("move ", destination);
    }
}

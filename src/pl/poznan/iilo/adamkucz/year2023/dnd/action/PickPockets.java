package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;
import pl.poznan.iilo.adamkucz.year2023.dnd.item.Item;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Container;

import java.util.Optional;

import static pl.poznan.iilo.adamkucz.year2023.dnd.world.World.WORLD;

public class PickPockets extends Action {
    public static final String NAME = "pickpockets";
    public final DndCharacter victim;
    public final Optional<Item> target;
    public final Optional<Container> place;

    public PickPockets(String line) {
        super(line);
        if (arguments.length < 1) {
            throw new IllegalArgumentException("No victim given to pick the pockets of!");
        }
        Optional<DndCharacter> specifiedVictim = WORLD.getByName(arguments[0]);
        if (!specifiedVictim.isPresent()) {
            throw new IllegalArgumentException("Invalid victim specification to pick the pockets of!");
        }
        victim = specifiedVictim.get();
        switch (arguments.length) {
            case 1:
                target = Optional.empty();
                place = Optional.empty();
                break;
            case 3:
                target = Optional.of(WORLD.getItem(arguments[1]));
                Optional<Item> maybeContainer = victim.getItem(arguments[2]);
                if (maybeContainer.isPresent() && maybeContainer.get() instanceof Container) {
                    place = Optional.of((Container) maybeContainer.get());
                } else {
                    throw new IllegalArgumentException("The victim doesn't have a " + arguments[2] + " to pickpocket!");
                }
            default:
                throw new IllegalArgumentException("Wrong number of arguments for the pickpocketing action!");
        }
    }
}

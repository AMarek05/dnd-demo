package pl.poznan.iilo.adamkucz.year2023.dnd.action;

public class MoveSilently extends Move {

    public static final String NAME = "movesilently";

    public MoveSilently(String line) {
        super(line);
    }
}

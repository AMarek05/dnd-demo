package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import java.util.Arrays;

public class Action {
    public final String actionname;
    public final String[] arguments;

    public Action(String line) {
        String[] elements = line.split(" ");
        if (elements.length < 1) {
            throw new IllegalArgumentException("The action needs to start with a name");
        }
        this.actionname = elements[0];
        this.arguments = Arrays.copyOfRange(elements, 1, elements.length);
    }
}

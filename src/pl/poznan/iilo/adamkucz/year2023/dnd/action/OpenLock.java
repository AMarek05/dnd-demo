package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Lock;

public class OpenLock extends TargetedAction<Lock> {
    public static final String NAME = "openlock";

    public OpenLock(String line) {
        super(line);
    }

    public OpenLock(Lock target) {
        super(NAME, target);
    }
}

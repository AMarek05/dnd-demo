package pl.poznan.iilo.adamkucz.year2023.dnd;

import pl.poznan.iilo.adamkucz.year2023.dnd.util.Util;

public class Dice {
    public static int roll(String spec) {
        String[] specParts = spec.split("d");
        int numberOfDice = Integer.parseInt(specParts[0]);
        int diceType = Integer.parseInt(specParts[1]);
        return roll(numberOfDice, diceType);
    }

    public static int roll(int numberOfDice, int diceType) {
        // functionally (advanced), the following line is enough
        // return Util.randomNumberGenerator.ints(numberOfDice, 1, diceType + 1).sum();
        int result = 0;
        for (int i = 0; i < numberOfDice; i++) {
            result += Util.randomNumberGenerator.nextInt(diceType) + 1;
        }
        return result;
    }

    public static int roll(int diceType) {
        return roll(1, diceType);
    }

    public boolean rollForChance(double percentage) {
        if (percentage == 1) {
            return true;
        }
        if (percentage == 0) {
            return false;
        }
        // convert double to int
        // only integer percentages accepted, so no 99.7 % (0.997)
        int cent = (int) Math.round(percentage * 100);
        return rollForPercent(valueAt(cent, 0), valueAt(cent, 1));
    }
    private int valueAt(int percentage, int index) {
        return String.valueOf(percentage).charAt(index);
    }
    private boolean rollForPercent(int firstCharacter, int secondCharacter) {
        // we assume that a d10 is from 0 to 9
        int firstRoll = roll(10) - 1;
        int secondRoll = roll(10) - 1;
        // we don't accept 0 0 , because that means 100% and thus will always yield false
        // in the first condition the first digit is equal, so we have to compare the second
        // for example for 10% the largest accepted roll will be 1 and 0
        // for 10 % also the smallest accepted roll will be 0 and 1
        // then it goes up to 0 and 9 and all in all we get that 10 out of a hundred options yield true.
        return ((firstRoll == firstCharacter && secondRoll <= secondCharacter)
                || (firstRoll < firstCharacter))
                && !(firstRoll == 0 && secondRoll == 0);
    }
}

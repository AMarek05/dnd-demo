package pl.poznan.iilo.adamkucz.year2023.dnd;

public class CharacterAbility {
    public int intelligence;
    public int wisdom;
    public int dexterity;
    public int constitution;
    public int charisma;

    public int strength;

    public CharacterAbility(int intelligence, int wisdom, int dexterity, int constitution, int charisma, int strength) {
        this.intelligence = intelligence;
        this.wisdom = wisdom;
        this.dexterity = dexterity;
        this.constitution = constitution;
        this.charisma = charisma;
        this.strength = strength;
    }

    @Override
    public CharacterAbility clone() {
        return new CharacterAbility(intelligence, wisdom, dexterity, constitution, charisma, strength);
    }
}

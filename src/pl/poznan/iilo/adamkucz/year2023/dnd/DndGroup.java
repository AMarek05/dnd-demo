package pl.poznan.iilo.adamkucz.year2023.dnd;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DndGroup<T extends DndBeing> {
    private final List<T> members;

    public DndGroup(T... members) {
        this.members = new ArrayList<>(Arrays.asList(members));

    }

    public void add(T character) {
        members.add(character);
    }

    public void addAll(T... beings) {
        members.addAll(Stream.of(beings).collect(Collectors.toCollection(ArrayList::new)));
    }

    public void moveTo(Location destination) {
        members.forEach(p -> p.moveTo(destination));
    }

    public T randomMember() {
        return members.get(new Random().nextInt(members.size()));

    }
}

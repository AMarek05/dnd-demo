package pl.poznan.iilo.adamkucz.year2023.dnd.item;

public class Money extends Item {
    public final int amount;
    public final CoinType type;

    public Money(int amount, CoinType type) {
        super(type.toString().toLowerCase());
        this.amount = amount;
        this.type = type;
    }

    public Money(int amount) {
        this(amount, CoinType.GOLD);
    }

    // returns the value of this stash of money in terms of gold pieces
    public double value() {
        switch (type) {
            case COPPER:
                return amount * 0.1 * 0.05;
            case SILVER:
                return amount * 0.05;
            case ELECTRUM:
                return amount * 0.5;
            case GOLD:
                return amount;
            case PLATINUM:
                return amount * 5;
            default:
                throw new IllegalStateException("Money with no specified type");
        }
    }

    public static Money none() {
        return new Money(0);
    }

    public enum CoinType {
        COPPER, SILVER, ELECTRUM, GOLD, PLATINUM
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.item;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.WorldElement;

public class Item extends WorldElement {
    private final String identifier;

    public Item(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Item copy() {
        return new Item(identifier);
    }
}

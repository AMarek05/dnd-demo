package pl.poznan.iilo.adamkucz.year2023.dnd.item;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Container;

public class Book extends Container {
    public final String language;

    public Book(String title, String language) {
        super("Book '" + title + "'");
        this.language = language;
    }
}

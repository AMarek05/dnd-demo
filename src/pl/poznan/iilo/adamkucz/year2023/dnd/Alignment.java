package pl.poznan.iilo.adamkucz.year2023.dnd;

import java.util.Objects;

import static pl.poznan.iilo.adamkucz.year2023.dnd.util.Util.capitalise;

public class Alignment {
    public final String order;
    public final String ethics;

    public final static String GOOD = "good";
    public final static String NEUTRAL_ETHICS = "neutral";
    public final static String EVIL = "evil";
    public final static String LAWFUL = "lawful";
    public final static String NEUTRAL_ORDER = "neutral";
    public final static String CHAOTIC = "chaotic";

    // both lowercase and uppercase spelling is allowed
    public Alignment(String order, String ethics) {
        this.order = order.toLowerCase();
        this.ethics = ethics.toLowerCase();
    }

    // assert Alignment.fromString(alignment.toString()).equals(alignment)
    public static Alignment fromString(String spec) {
        String[] elements = spec.split(" ");
        if (elements.length != 2) {
            throw new IllegalArgumentException("Wrong specification of alignment: " + spec);
        }
        String order = elements[0].toLowerCase().equals("true") ? "neutral" : elements[0];
        return new Alignment(order, elements[1]);
    }

    public String language() {
        if (order.equals("neutral") && ethics.equals("neutral")) {
            return "Neutrality";
        }
        return this.toString();
    }

    @Override
    public String toString() {
        if (order.equals("neutral") && ethics.equals("neutral")) {
            return "True Neutral";
        }
        return capitalise(order) + " " + capitalise(ethics);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Alignment)) {
            return false;
        }
        Alignment otherAlignment = (Alignment) other;
        return Objects.equals(this.order, otherAlignment.order) &&
                Objects.equals(this.ethics, otherAlignment.ethics);
    }
}

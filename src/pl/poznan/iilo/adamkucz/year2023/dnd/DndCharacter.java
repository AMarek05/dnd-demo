package pl.poznan.iilo.adamkucz.year2023.dnd;

import pl.poznan.iilo.adamkucz.year2023.dnd.action.Action;
import pl.poznan.iilo.adamkucz.year2023.dnd.characterclass.CharacterClass;
import pl.poznan.iilo.adamkucz.year2023.dnd.spell.Spell;
import pl.poznan.iilo.adamkucz.year2023.dnd.util.Util;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Location;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.WorldElement;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

public class DndCharacter extends DndBeing {
    private int experience;
    private int level;

    private CharacterAbility ability;


    private final int initialConstitution;

    private CharacterClass characterClass;
    private Alignment alignment;

    private String languageBeingLearned;

    private Spell[] preparedSpells;

    public DndCharacter(String name, CharacterAbility ability, CharacterClass characterClass, Alignment alignment) {
        super(name, characterClass.initialHp());
        this.ability = ability.clone();
        this.alignment = alignment;
        this.initialConstitution = ability.constitution;

        if (!characterClass.isAllowed(alignment, ability)) {
            throw new IllegalArgumentException("Invalid combination of alignment, abilities, and class");
        }
        this.characterClass = characterClass;
        this.languageBeingLearned = null;

        experience = 0;
        preparedSpells = new Spell[]{};

        learnLanguage(alignment.language());
        characterClass.applyInitialSpecialModifiers(this);
    }

    // identifier should be of the form "Name the alignment class", e.g. "Malcolm the True Neutral Wizard"
    public static DndCharacter createCharacterWithAbilities(String identifier, CharacterAbility abilities) {
        // this method uses advanced Java magic, you are *not* encouraged to try to understand it
        String[] identifierParts = identifier.split(" ");
        if (identifierParts.length != 5) {
            throw new IllegalArgumentException(
                    "The character specification for '" + identifier + "' is in an incorrect format");
        }
        String name = identifierParts[0];
        String className = identifierParts[4];
        try {
            Class<?> typeOfCharacterClass = Class.forName(className);
            Constructor<?> constructor = typeOfCharacterClass.getConstructor();
            Object characterClassCandidate = constructor.newInstance();
            if (characterClassCandidate instanceof CharacterClass) {
                CharacterClass characterClass = (CharacterClass) characterClassCandidate;
                Alignment alignment = Alignment.fromString(identifierParts[2] + " " + identifierParts[3]);
                return new DndCharacter(name, abilities, characterClass, alignment);
            } else {
                throw new ClassNotFoundException();
            }
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(
                    "Cannot create " + identifier + ", class '" + className + "' is unknown", e);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(
                    "Cannot create " + identifier + ", you cannot simply create " + className + "s", e);
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void learnLanguage(String language) {
        if (!knowsLanguage(language)) {
            knownLanguages = Util.extendArray(knownLanguages, language);
        }
    }

    // Time for a new language to be learned, in months
    public int timeToLearn(String language, DndCharacter instructor) {
        if (!instructor.knowsLanguage(language)) {
            return -1;
        }
        return Math.min(12, 12 - (ability.intelligence - 12));
    }

    public int getLevel() {
        return level;
    }

    @Override
    public void doAction(Action action) {
        if (characterClass.doAction(this, action)) {
            return;
        }
        super.doAction(action);
    }

    public CharacterAbility getAbility() {
        return ability.clone();
    }
}

package pl.poznan.iilo.adamkucz.year2023.dnd.monster;

import pl.poznan.iilo.adamkucz.year2023.dnd.util.RandomValue;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Speed;

public class Orc extends Monster {

    public Orc() {
        // TODO: implement a EquippedMonster class for monsters that deal damage with weapons
        super("orc", 1, 6, new Speed(9), 1, new RandomValue(1, 8));
    }

    // copy constructor
    public Orc(Orc other) {
        super(other);
    }

    @Override
    public Orc[] copy(int n) {
        // advanced (functional)
        // return Stream.generate(() -> new Orc(this)).limit(n).toArray(Orc[]::new);
        Orc[] arr = new Orc[n];
        for (int i = 0; i < n; i++) {
            arr[i] = new Orc(this);
        }
        return arr;

    }
}

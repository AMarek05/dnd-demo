package pl.poznan.iilo.adamkucz.year2023.dnd.monster;

import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;
import pl.poznan.iilo.adamkucz.year2023.dnd.DndBeing;
import pl.poznan.iilo.adamkucz.year2023.dnd.util.RandomValue;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Speed;

public class Monster extends DndBeing {
    private final RandomValue hpRange;
    private int armorClass;
    private Speed speed;
    private int numberOfAttacks;
    private RandomValue attackDamage;

    // TODO: implement all features described in monster manual pages 5-6
    // NOTE: frequency and number appearing logically do not belong to this class
    //   because they're not properties of any individual monster
    //   logically, they could be properties of locations or of monster species
    // similarly, % in lair logically should be implemented as an attribute of Lair class, not Monster
    public Monster(String name, RandomValue hpRange, int armorClass, Speed speed,
                   int numberOfAttacks, RandomValue attackDamage) {
        super(name, hpRange.roll());
        this.hpRange = hpRange;
        this.armorClass = armorClass;
        this.speed = speed;
        this.numberOfAttacks = numberOfAttacks;
        this.attackDamage = attackDamage;
    }

    public Monster(String name, int hitDiceNumber, int armorClass, Speed speed,
                   int numberOfAttacks, RandomValue attackDamage) {
        this(name, new RandomValue(hitDiceNumber, 8, 0), armorClass, speed, numberOfAttacks, attackDamage);
    }

    // copy constructor
    public Monster(Monster other) {
        super(other);
        this.hpRange = other.hpRange;
        this.armorClass = other.armorClass;
        this.speed = other.speed;
        this.numberOfAttacks = other.numberOfAttacks;
        this.attackDamage = other.attackDamage;
    }

    public Monster[] copy(int n) {
        // advanced (functional)
        // return Stream.generate(() -> new Monster(this)).limit(n).toArray(Monster[]::new);
        Monster[] arr = new Monster[n];
        for (int i = 0; i < n; i++) {
            arr[i] = new Monster(this);
        }
        return arr;
    }
}
